const Path = require('path');
const Hapi = require('hapi');
const Inert = require('inert');
const _ = require('lodash');
const JsonDB = require('node-json-db');

const db = new JsonDB("myDataBase", true, true);

const config = {
    
};
const server = new Hapi.Server(config);
//server.connection({ port: 3000, host: '192.168.0.114' });
server.connection({ port: 3000, host: '192.168.0.14' });
//server.connection({ port: 3000, host: '192.168.1.66' });

const Bcrypt = require('bcrypt');

const users = {
    amanda: {
        username: 'amanda',
        password: '$2a$04$chYZRad7p2NPQi3uWPQu6.dF93YDnKJ5JW3fbrCiSjECbd1Hq6K02',   // 'secret'
        name: 'amanda',
        id: '2133d32a'
    }
};

function validate(request, username, password, callback) {

    const user = users[username];
    if (!username) {
        return callback(null, false);
    }

    Bcrypt.compare(password, user.password, (err, isValid) => {

        callback(err, isValid, { id: user.id, name: user.name });
    });
};

server.register([Inert, require('hapi-auth-basic')], (err) => {
    if (err) {
        throw err;
    }

    server.auth.strategy('simple', 'basic', { validateFunc: validate });

    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: 'public',
                index: true
            }
        },
        config: {
            auth: 'simple'
        }
    });

    server.route({
        method: 'GET',
        path: '/db/{param*}',
        config: {
            auth: 'simple',
            handler: (request, reply) => {
                console.log(`GET ${request.params.param}`);
                const path = _.isNil(request.params.param) ? '' : request.params.param;
                reply(db.getData('/meals'+path));
            }
        }
    });

    server.route({
        method: ['POST', 'PUT'],
        path: '/db/{param*}',
        config: {
            auth: 'simple',
            handler: (request, reply) => {
                try {
                    if (_.isEmpty(request.params.param)) {
                        throw new Error('Path must not be empty.');
                    }
                    db.push('/meals'+request.params.param, request.payload);
                    if (request.params.param === '[]') {
                        reply(request.payload).code(request.method == 'post' ? 201 : 200);
                    } else {
                        reply(db.getData('/meals'+request.params.param)).code(request.method == 'post' ? 201 : 200);
                    }
                } catch(err) {
                    console.error(err);
                    reply(err);
                }
            }
        }
    });

    server.route({
        method: 'DELETE',
        path: '/db/{param*}',
        config: {
            auth: 'simple',
            handler: (request, reply) => {
                try {
                    if (_.isEmpty(request.params.param)) {
                        throw new Error('Path must not be empty.');
                    }
                    console.log(`DELETE ${request.params.param}`);
                    db.delete('/meals'+request.params.param);
                    reply(db.getData(request.params.param)).code(204);
                } catch(err) {
                    console.error(err);
                    reply(err);
                }
            }
        }
    });

});

server.start((err) => {

    if (err) {
        throw err;
    }

    console.log('Server running at:', server.info.uri);
});